import { Box, Typography, Avatar, Button, TextField } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { useState } from 'react';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [isEdit, seIsEdit] = useState(false);
  const [newName, setName] = useState(name);
  const [newEmail, setEmail] = useState(email);

  return (
    <Card sx={sx}>
      <Button sx={{ fixed: 'right' }} onClick={() => seIsEdit(!isEdit)}>
        Edit
      </Button>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          {isEdit ? (
            <Box display="flex" flexDirection="column" alignItems="center">
              <TextField
                placeholder="name"
                value={newName}
                onChange={(e) => setName(e.target.value)}
              />
              <TextField
                placeholder="email"
                value={newEmail}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button>Submit</Button>
            </Box>
          ) : (
            <>
              <Typography variant="subtitle1" lineHeight="1rem">
                {name}
              </Typography>
              <Typography variant="caption" color="text.secondary">
                {email}
              </Typography>
            </>
          )}
        </Box>
      </Box>
    </Card>
  );
};
